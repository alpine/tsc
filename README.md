# Technical Steering Committee

This project is used to track the work of the technical steering committee
(@team/tsc). It hosts the [meeting minutes](minutes/), the agenda for the next meeting,
and allow the community to propose changes.
