# Technical Steering Committee Workflow

## Introduction

The Alpine Linux Technical Steering Committee (TSC) is responsible for

* setting the technical direction and guidelines for the project
* approving changes that either
    * have a significant impact to Alpine Linux users
    * affect a number of packages, or
    * are potentially controversial
* maintaining a technical roadmap
* coordinating the work delegated to various technical Special Interest Groups
  (SIGs), and
* technical matters which are not in the scope of any particular SIG.

The [Alpine Linux Council](https://gitlab.alpinelinux.org/alpine/council)
appoints the members of the TSC. The TSC defines what SIGs exist, has
representation in each SIG, and appoints the leader of each SIG.

This document is the authoritative description on how the TSC operates.

## Raising Issues to the Committee

Anyone in the Alpine Linux community can raise an issue to the TSC by creating
an issue for the [TSC GitLab
project](https://gitlab.alpinelinux.org/alpine/tsc). The issue will eventually
be handled in an upcoming TSC meeting.

## Meeting Practices

The TSC convenes every second week to handle the [open
issues](https://gitlab.alpinelinux.org/alpine/tsc/-/issues). Each meeting is
scheduled to last 45 minutes and takes place via a video conferencing system.
The meetings are not recorded.

Between the meetings, the members are encouraged to comment the open issues in
GitLab, especially the issues that are on the agenda of the next meeting. In
this way, the meeting time can be used more effectively.

### Officials and Minutes

Each meeting has two officials: the *Chair* and the *Scribe*. Each TSC member
acts in both roles in an alphabetical round-robin fashion. The Scribe shall
become the Chair of the next meeting. If the Chair or the Scribe does not show
up at the meeting, the TSC elects substitutes as needed.

The responsibilities of the Chair include:

* prioritizing and tagging the open issues before the meeting
* scheduling the meeting such that the time is suitable for himself, the
  Scribe, and most of the members
* sending out a meeting invitation to the other members by e-mail at least 7
  days prior to the meeting
* chairing the meeting
* organizing a vote if consensus cannot be reached
* ensuring a common understanding of what was decided after each item handled,
  and
* verifying the correctness of the meeting minutes.

The Scribe is responsible for:

* writing the meeting minutes, accurately recording what was decided, including
  who shall implement the decision
* pushing the minutes to the [GitLab
  repository](https://gitlab.alpinelinux.org/alpine/tsc/-/tree/master/minutes)
  within 3 days of the meeting, and
* updating the status of the handled issues by writing comments about the TSC's
  position when necessary.

Members who were present in the meeting may suggest revisions to the minutes by
opening merge requests. Unless having been resolved earlier, these merge
requests shall be handled as the first agenda item in the next meeting.

### Quorum and Voting

Each TSC meeting begins by determining if there is a quorum. The quorum is 5
people, and if a decision is controversial it is recommended that more than 5
people are present. If too few members show up, the start of the meeting is
delayed by up to 15 minutes. If no quorum can be reached within that time,
the meeting is postponed.

When unable to attend a meeting due to an exceptional schedule or unforeseen
circumstances, a member is expected to notify the Chair as soon as possible.
If the Chair knows in advance that there will be no quorum, he is expected to
postpone the meeting.

When making decisions, the TSC attempts to achieve a consensus by thoroughly
discussing the matter. If no consensus can be reached, the Chair organizes a
vote. The opinion of the majority becomes the TSC's position. When determining
the majority, the members who refrained from voting are considered absent.

In case of a tie, the issue is escalated to the Council. If all Council members
participated in the TSC vote, the decision gets immediately made based on their
votes without a deferral to the next Council meeting.

If there are more than two options to vote for, it is up to the Chair to decide
whether to use sequential elimination, multi-option voting, two-round system,
or other procedures. However, no decision can be made without

* the backing of a simple majority of those who cast their votes, or
* a tie-break by the Council when half of the votes supported the decision.

### Inviting Experts

When necessary, the Chair can invite to the meeting one or more subject matter
experts who are not members of the TSC. For such meetings, a one-time URL shall
be used instead of the regular TSC meeting URL. These experts cannot vote in
the meeting.

### Agenda

On the [agenda board](https://gitlab.alpinelinux.org/alpine/tsc/-/boards/697),
the open issues are arranged into columns according to `agenda` tags. The
issues in each column are arranged in a priority order such that the topmost
issue is considered the most important. When prioritizing the issues, the Chair
pays attention to the importance, the urgency, and the age of the issues.

The issues tagged with `agenda:scheduled` are expected to be handled in the
next meeting in their priority order. At most 3 items can have this tag
attached at the same time. At his discretion, the Chair may also request status
updates on any issue with an `agenda:follow-up` tag. Hence, each meeting
follows this agenda:

1. Opening of the meeting
1. Confirming the meeting officials
1. Determining if a quorum is present
1. Reviewing any open [merge
   requests](https://gitlab.alpinelinux.org/alpine/tsc/-/merge_requests)
1. Status update on selected `agenda:follow-up` issues
1. First `agenda:scheduled` issue
1. Second `agenda:scheduled` issue
1. Third `agenda:scheduled` issue
1. Closing of the meeting

The `agenda` tags are expected to be up-to-date when the Chair sends out the
invitation. If there are good reasons, the Chair may revise them afterwards.
Only in exceptional cases the Chair would revise the agenda less than 3 days
prior to the meeting.

At least 10 minutes must be reserved for each `agenda:scheduled` item. When one
item is finished and there is less than 10 minutes time left, the Chair closes
the meeting, deferring the remaining items to a future meeting.

## Changes to This Workflow

Any proposed change to this document shall be submitted as a merge request,
which is handled as a TSC agenda item according to the normal procedure.
