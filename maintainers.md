# Becoming a Maintainer

You may become a package maintainer by opening a merge request that contains
your name and e-mail address in the `Maintainer` field of a new or an existing
`APKBUILD` file.

New packages shall initially be placed in the `testing` repository. You are
expected to move the package to the `community` repository within a few weeks,
once you have sufficiently tested it.

# Maintainer Responsibilities

* providing fresh upstream releases on edge, preferably the latest release but
  also considering
    * support lifecycle compatibility between upstream and AL policy, and
    * compatibility with depending packages
* on edge and all stable branches under active maintenance:
    * fixing issues related to building and compatibility with the base system
    * backporting critical bug fixes from upstream
    * fixing major security issues promptly
* adherence to TSC decisions and policies

# Maintainer Privileges

* delegation of maintenance work to others who volunteer to help
* rejecting non-trivial changes
* reverting commits

## Non-Maintainer Updates

Generally, directly pushing commits to packages maintainted by someone else
should be avoided. Opening a merge request on the proposed update gives the
maintainer a chance to review it in advance.

Direct pushes may be used when the update is urgent or when creating
package-specific merge requests would require disproportionate effort. Such
situations include

* security patching
* large updates affecting multiple packages in a similar way, and
* fixing broken builds.

# Resigning as the Maintainer

If you no longer want to maintain a package, please announce your resignation
on the `~alpine/devel` mailing list. This will result in

* someone else picking up the maintainership, or
* the removal of the package and its dependent packages.

## Demoting Packages

A similar announcement should be made if you intend to demote your package from
the `main` repository to `community`. This will give other developers a chance
to assume the maintainership, in case they prefer to keep the package and its
dependent packages under the longer maintenance cycle.
