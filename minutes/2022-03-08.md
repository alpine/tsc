# Meeting Minutes: Techincal Steeting Committee

Date: 08 March 2022, 16:15 UTC

Present:

* Natanael Copa (chair)
* Kevin Daudt (scribe)
* Francesco Colista
* Kaarle Ritvanen
* Nathan Angelacos
* Sören Tempel
* Ariadne Conill

Absent:

* Carlo Landmeter
* Timo Teräs
* Leonardo Arena

## System change proposal: Rust in main (#21)

Will be postponed until 3.17, as we do not have rust on s390x yet.

## Planning for 3.16 release (#40)

What can we realistically include and focus on?

@ncopa will send an e-mail to the community with what we want to include and
asks for other suggestions

### APKv3

Main blocker is modifying abuild to generate APKv3 packages through `apk
makepkg`. Needs to be adjusted to take a APKv2 control file instead of taking
arguments from the CLI. @ariadne tries to work on it next week. Waiting on @fabled
to make the release after that.

It might not be realistic to have it in 3.16. The release of apk3 might be after
the release freeze. @ncopa suggested the freeze may be postponed a bit to
include APKv3

### Modular kernel configs

Could be improved significantly with some simple steps. @ncopa thinks he can
spend halve a day on it.

### Cloud images

@clandmeter is working on something. The cloud team worked on an image for
Oracle cloud.

### musl release

Waiting for upstream to do an actual release, should be trivial to upgrade once
released.

@ariadne will ping Rich Felker to ask to do it before our release.

### Drop python2

Only qt5-qtwebengine  and qt6-qtwebengine rely on it. @nmeum will look to see if
it can be patched. May not be possible in time.

### replace ash (#39)?

Realistically not for 3.16. Possible alternative is mrsh.

### openssl3/libressl (#28)

Not realistic to switch to libressl. We will try to switch to openssl3.0 for
3.16 (lead by @psykose).

We defer the decision to switch to LibreSSL to a later moment.

## consider LibreSSL as default OpenSSL provider again (#28)

Postpone until a later time

## [3.16] replace busybox ash with another /bin/sh (#39)

Postpone until after 3.16

## Update CoC (#41)

@ncopa wants to know what the opinion of the TSC is about the suggestions for
the CoC. Ultimately it will be the council that will make the decision.

No very strong opinions on what CoC will be used. There is concensus that the
Contributor Covenant suffices. The scope should in general be limited to what
people do in the Alpine Linux community.

## Next meeting

* Chair: Kevin Daudt
* Scribe: Carlo Landmeter


