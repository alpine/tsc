# Alpine TSC Meeting - 2 August 2022

## Attendees

* Francesco Colista
* Ariadne Conill
* Natanael Copa
* Kevin Daudt
* Kaarle Ritvanen
* Soren Tempel

## Replace Busybox ash with another /bin/sh (#39)

* Ariadne suggested FreeBSD /bin/sh as a package to test with.
* Soren suggested doing a size comparison.
* Followup at next meeting.

## Drop s390x (#44)

* Main reason for dropping s390x was lack of Rust.
* An architecture maintenance team has been suggested by Soren.
* Soren: Quality of the port could be improved.
* As Rust is now available, the issue will be closed.

## Rework device manager handling, split busybox-initscripts (#52)

* As an initial step, move `busybox-initscripts` to `busybox` package
  with `-openrc`.  Same for `busybox-extras-openrc`.  Ariadne volunteered
  to do this.
* Work on a regression testing framework for mdev.
* Reinvestigate the `dev-initscripts` proposal at a later meeting.
