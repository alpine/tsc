# Meeting Minutes: Technical Steering Committee

Date: June 16, 2022

Time: 15:15–16:00 UTC

Present:

* Nathan Angelacos
* Leonardo Arena (scribe)
* Francesco Colista
* Natanael Copa
* Kevin Daudt
* Carlo Landmeter
* Sören Tempel (chair)

Absent:

* Kaarle Ritvanen
* Timo Teräs
* Ariadne Conill

## Ban indiscriminate setcap usage ([#45](https://gitlab.alpinelinux.org/alpine/tsc/-/issues/45))

* Detect capabilities set via abuild
* Remove executable attribute for everyone

## Introduce standard subpackage -man for man pages ([#16](https://gitlab.alpinelinux.org/alpine/tsc/-/issues/16))

To be applied only for doc subpackages which size is above a defined threshold.

To be defined next time:

* Size threshold
* Subpackage name: "doc-extra" or "man" subpackage

## Consider asynchronous TSC meetings ([#49](https://gitlab.alpinelinux.org/alpine/tsc/-/issues/49))

* Quorum reduced to 5 people
* Add more time slots to allow everyone a chance to participate
