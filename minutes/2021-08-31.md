# Meeting Minutes: Technical Steering Committee

Date: August 31, 2021

Time: 15:15–16:00 UTC

Present:

* Carlo Landmeter
* Kevin Daudt
* Ariadne Conill
* Francesco Colista
* Kaarle Ritvanen
* Leonardo Arena
* Natanael Copa
* Sören Tempel (Scribe)

## Discussion regarding TSC Schedule

* Agreed on shifting meetings to two days so everybody can attend
* Agreed that we need half + 1 rounded down to be present for the TSC to make decisions
* Move meeting agenda decision to next week

## Future of eudev

See https://gitlab.alpinelinux.org/alpine/tsc/-/issues/12

* Problem: eudev is no longer maintained by Gentoo
* Proposed solutions:
	1. Find a new maintainer for eudev
	2. Do what Gentoo is doing, i.e. build udev from systemd source tree with OpenEmbedded patches
	3. Switch to something else, libudev-zero is suggested in this regard
* It is suggested to do what Gentoo is doing for now and mid to long term
  improve libudev-zero and make it a more viable alternative in the meantime
	* It is also suggested to make it possible to install both at the
	  same time (which is not possible at the moment) to allow
	  testing libudev-zero
* Problem: Upcoming Alpine release
	* Do we wan to continuing support an eudev for 2 years or are we
	  switching to the Gentoo approach for 3.15?

* We are investigating whether we can take the Gentoo approach and in
  parallel also improve the libudev-zero package (at least ensure that
  they can be installed at the same time for testing purposes)

## alpine-glibc

See https://gitlab.alpinelinux.org/alpine/tsc/-/issues/17

* Some people are taking the Alpine base system and integrate glibc with it
* This can cause a lot of problems due to ABI differences between glibc and musl et cetera
* We have to somehow clarify that we don't support using musl and glibc
  in the same Alpine container image
	* One proposal for doing so: Making the musl package conflict
	  with the glibc package
	* Alternative: Documenting the various ways one can break an
	  Alpine system (including mixing musl with glibc)

* It is preferred to handle this through documentation instead of
  introducing a package conflict
* The maintainer of the glibc package also offered adding a notice to
  his project page

* Details will be deferred to the documentation team

## Next Meeting

* Next TSC meeting is on 13.09
	* Chair: Sören
	* Scribe: Timo

* The agenda for the next meeting should be kept short
	* A maximum of three items is suggested
