# Meeting Minutes: Technical Steering Committee

Date: September 13, 2021

Time: 16:00–17:00 UTC

Present:

* Nathan Angelacos
* Natanael Copa (Chair)
* Ariadne Conill
* Carlo Landmeter
* Timo Teräs (Scribe)
* Kaarle Ritvanen

Absent:

* Leonardo Arena
* Francesco Colista
* Kevin Daudt
* Sören Tempel

## Deadline for packages in testing

See https://gitlab.alpinelinux.org/alpine/tsc/-/issues/2

* Many things go to testing, and are never touched again. Suggestion: have deadline
  to move packages to community or remove from testing. This is to encourage proper
  maintenance of packages.
* Historically testing was used only to test that things work in builder, and were
  often moved immediatelly to comminity after successful build.
* Sometimes the package's LTS is in community, and latest release in testing.
  Those packages could be tagged as 'should stay in testing forever'.
* Discussion whether to move dead package to unmainted or delete. Deletion preferred.
* Decision: after 3 months, send notification to maintainer or the committed;
  after 6 months deletion.
* Decision: start with manual deletion of stale packages with target to automate.
  Add this as a step in release procedure. Carlo will document the process, and
  do or delegate (e.g. Leo or Francesco) the first clean up.
* Define a process to move from testing to community.

## Define process for moving things from main to community

See https://gitlab.alpinelinux.org/alpine/tsc/-/issues/8

* Triggered by gpsd move to community.
* The maintainer might be willing to extend support for the package even if the
  upstream package does not provide support required for main.
* Discussion on having more repositories than main/community. But categorizing
  packages can be difficult.
* Write clear definition what goes to main (support provided for two years after
  release), and what goes to community (limited support after release).
* Decision: process is to ask maintainer firs, and he is unwilling to commit
  then security team. When moving from community to main, consult security team.
* Kaarle will create ticket to discuss responsibilites and privileges of a maintainer,
  and how to handle non-maintainer updates/changes.
  
## Define the scope of TSC

See https://gitlab.alpinelinux.org/alpine/tsc/-/issues/11

Moved to next meeting due to running out of time, and to have more member present.

## Next Meeting

* Next TSC meeting is on Tuesday 28.09.
	* Chair: Timo
	* Scribe: Nathan
* Decided to include #11 as part of agenda
